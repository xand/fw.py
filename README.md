# fw.py

Small python application for generating iptables rules.

This script follows basic principle. __Whatever is not allowed is forbidden__. This means that even the outgoing connections are forbidden by default.

Also this script implements the technique called `port knocking`. You may find more information [here](https://www.digitalocean.com/community/tutorials/how-to-configure-port-knocking-using-only-iptables-on-an-ubuntu-vps).

## Installation

Before installing this script, make sure you have python 3 installed. If not, install it normally for your operating system.

The installation is pretty simple. All commands should be performed as root since we are installing new services in the system.

- Clone the repository into `/opt`.
```bash
$ sudo mkdir -p /opt && cd /opt
$ sudo git clone https://gitlab.com/xand/fw.py.git
```

- Run `install.sh` script as root.
```bash
$ sudo /opt/fw.py/install.sh
```

## Usage

The script is intended to run on system boot with Upstart boot system (used mainly on Ubuntu).

For test purposes you may run it with:
```bash
sudo service fw.py start|stop|restart
```

## Configuration

Before running this script you should update the configuration with desired values.

- DRY_RUN. do not perform any action, just output iptables instructions. Values: `True` or `False`.

- WHITELIST_IPS. array of ip addresses with allowed access. Addresses listed here will be allowed to access any port. Example: `['192.168.1.1', '8.8.8.8']`.

- GREEN_INTERFACES. Array of interfaces with allowed access. Example: `['lo', 'eth1']`. __Please note__: you should always allow `lo` interface.

- ALLOWED_INPUT. An array of objects defining allowed ip addresses to access port. Each object must have the following structure:
  - iface. Interface through which the connection is established. __This parameter is mandatory__. Maybe any interface from `ifconfig` command output. For example: `eth1`, `eth2`. 
  - port. Port number to which you want to allow access. __This parameter is optional__.
  - addr. Source address from which the connection is established. __This parameter is optional__.
  - proto. Protocol used for connection. __This parameter is optional__. Allowed values are: `tcp`, `udp`.

  Example:

  `{'iface': 'eth2', 'port': 80, 'addr': '192.168.35.188', 'proto': 'tcp'}`

  In this case `tcp` connections made through `eth2` interface and from address `192.168.35.188` to HTTP port (`80`) will be allowed.

- ALLOWED_OUTPUT. An array of objects defining allowed output. Each object must have the following structure:
  - iface. Interface through which the connection is established. __This parameter is mandatory__. Maybe any interface from `ifconfig` command output. For example: `eth1`, `eth2`.
  - port. Port number to which the connection will be established. __This parameter is optional__.
  - addr. IP address to which the connection will be established. __This parameter is optional__.
  - proto. Protocol used for connection. __This parameter is optional__. Allowed values are: `tcp`, `udp`.

  Example:

  `{'iface': 'eth2', 'port': 53, 'addr': '192.168.35.1', 'proto': 'udp'}`

  This object allows `udp` connections to `192.168.35.1:53` made throught `eth2` interface.

- INPUT_DROP_NO_LOG. If you log gets overfilled with dropped packets you may configure `drop silently` with this option. This is an array of objects defining input which is silently dropped, I mean, with no log trace. Each object must have the following structure:
  - iface. Interface through which the connection is established. __This parameter is mandatory__. Maybe any interface from `ifconfig` command output. For example: `eth1`, `eth2`.
  - port. Port number to which remote system tries to establish the connection. __This parameter is optional__.
  - addr. IP address of the remote system which is trying to establish the connection. __This parameter is optional__.
  - proto. Protocol used for connection. __This parameter is optional__. Allowed values are: `tcp`, `udp`.

  Example:

  `{'iface': 'eth2', 'port': 1234, 'addr': '192.168.35.1', 'proto': 'udp'}`

  With this configuration we will silently drop all `udp` connections from `192.168.35.1` going to port `1234`.

- FORWARD_POLICY. This object defines the policy for forwarding ip packets. The configuration is pretty straightforward. You define the input/output interfaces which are allowed to traverse the firewall. Each object has a structure with one of the following:
  - in_iface. Allowed input interface.
  - out_iface. Allowed output interface.

  Example:

  `{'in_iface': 'tun0'}`

  With this configuration the packets arriving to `tun0` interface are allowed to continue their path.

- SSH_KNOCKING_SEQ. Port sequence for SSH access by port knocking. You may define any port sequence here. For example, if you define a sequence like that `[1001, 1002, 1003]` you will need to knock ports 1001, 1002, 1003 in that order, after doing this your ip will be allowed to perform SSH connection during 30 seconds.

- ALLOW_OUTGOING_PING. Set to True if you want to allow outgoing ping requests. Allowed values are `True` or `False`.

- ALLOW_INCOMING_PING. Set to True if you want to allow incoming ping requests. Allowed values are `True` or `False`.


## Performing port knocking

Along with the script there is `knock-example.sh` file. This shell script shows basic example for port knocking with `nmap` utility. Of course, you may use any utility available out there for performing this operation.

```bash
#!/bin/bash

nmap -Pn --host_timeout 201 --max-retries 0 -p 1001 192.168.35.123
nmap -Pn --host_timeout 201 --max-retries 0 -p 1002 192.168.35.123
nmap -Pn --host_timeout 201 --max-retries 0 -p 1003 192.168.35.123

ssh ubuntu@192.168.35.123
```

### Note on port knocking

When using old version of libpcap (library nmap uses for its operations), sometimes port knocking stops working. Affected versions are < 1.7.3.
You can get your version with the command `nmap -V`.
This is due to the fact that this version sends two TCP packets instead of one.

More information here:
- [Server fault](http://serverfault.com/questions/693093/why-does-nmap-send-two-packets-in-order-to-test-a-single-port).
- [Bug](https://github.com/nmap/nmap/issues/34) opened for nmap on Github.

