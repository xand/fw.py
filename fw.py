#!/usr/bin/python3

import json
import os

from subprocess import call
from collections import namedtuple

I = '/sbin/iptables'


def build_forward_chain(config):
    for i in config.FORWARD_POLICY:
        if hasattr(i, 'in_iface'):
            my_call(['-A', 'FORWARD', '-i', i.in_iface, '-j', 'ACCEPT'])
        elif hasattr(i, 'out_iface'):
            my_call(['-A', 'FORWARD', '-o', i.out_iface, '-j', 'ACCEPT'])
    
    my_call(['-A', 'FORWARD', '-j', 'LOG', '--log-prefix', 'iptables-fw-log ',  '--log-level', '4'])


def build(config):
    # flush everything
    my_call(['-F'])
    my_call(['-X'])
    my_call(['-Z'])
    my_call(['-t', 'nat', '-F'])

    build_forward_chain(config)
    
    # allow green interfaces to do anything
    for i in config.GREEN_INTERFACES:
        my_call(['-A', 'INPUT', '-i', i, '-j', 'ACCEPT'])
        my_call(['-A', 'OUTPUT', '-o', i, '-j', 'ACCEPT'])
        
    # allow for allowed output
    for i in config.ALLOWED_OUTPUT:
        arr = ['-A', 'OUTPUT', '-o', i.iface]
        if 'addr' in i:
            arr += ['-d', i.addr]
            
        if 'proto' in i:
            arr += ['-p', i.proto]

        if 'port' in i:
            arr += ['--dport', str(i.port)]
            
        arr += ['-j', 'ACCEPT']
    
        my_call(arr)

    # allow output for established connections
    my_call(['-A', 'OUTPUT', '-m', 'conntrack', '--ctstate', 'RELATED,ESTABLISHED', '-j', 'ACCEPT'])

    if config.ALLOW_OUTGOING_PING:
        my_call(['-A', 'OUTPUT', '-p', 'icmp', '--icmp-type', '8', '-m', 'state', '--state', 'NEW,ESTABLISHED,RELATED', '-j', 'ACCEPT'])

    if config.ALLOW_INCOMING_PING:
        my_call(['-A', 'OUTPUT', '-p', 'icmp', '--icmp-type', '0', '-m', 'state', '--state', 'ESTABLISHED,RELATED', '-j', 'ACCEPT'])
    
    # log everything else what tries to get out
    my_call(['-A', 'OUTPUT', '-j', 'LOG', '--log-prefix', 'iptables-output-log ', '--log-level', '4'])

    # allow input for established connections
    my_call(['-A', 'INPUT', '-m', 'conntrack', '--ctstate', 'RELATED,ESTABLISHED', '-j', 'ACCEPT'])

    # allow whitelisted ips
    for i in config.WHITELIST_IPS:
        my_call(['-A', 'INPUT', '-s', i, '-j', 'ACCEPT'])

    build_ssh_knocking(config)
        
    # allow for allowed input
    for i in config.ALLOWED_INPUT:
        arr = ['-A', 'INPUT', '-i', i['iface']]
        if 'addr' in i:
            arr += ['-s', i.addr]

        if 'proto' in i:
            arr += ['-p', i.proto]

        if 'port' in i:
            arr += ['--dport', str(i.port)]

        arr += ['-j', 'ACCEPT']
    
        my_call(arr)

    if config.ALLOW_OUTGOING_PING == 'True':
        my_call(['-A', 'INPUT', '-p', 'icmp', '--icmp-type', '0', '-m', 'state', '--state', 'ESTABLISHED,RELATED', '-j', 'ACCEPT'])

    if config.ALLOW_INCOMING_PING == 'True':
        my_call(['-A', 'INPUT', '-p', 'icmp', '--icmp-type', '8', '-m', 'state', '--state', 'NEW,ESTABLISHED,RELATED', '-j', 'ACCEPT'])

    # discard specified packets
    for i in config.INPUT_DROP_NO_LOG:
        arr = ['-A', 'INPUT', '-i', i.iface]
        if 'addr' in i:
            arr += ['-s', i.addr]
            
        if 'proto' in i:
            arr += ['-p', i.proto]

        if 'port' in i:
            arr += ['--dport', str(i.port)]
            
        arr += ['-j', 'DROP']
    
        my_call(arr)
        
    # log everything else
    my_call(['-A', 'INPUT', '-j', 'LOG', '--log-prefix', 'iptables-input-log ', '--log-level', '4'])

    # default drop everything
    my_call(['-P', 'INPUT', 'DROP'])
    my_call(['-P', 'OUTPUT', 'DROP'])
    my_call(['-P', 'FORWARD', 'DROP'])

def build_ssh_knocking(config):
    if len(config.SSH_KNOCKING_SEQ) == 0:
        return
    
    # for info please see: https://www.digitalocean.com/community/tutorials/how-to-configure-port-knocking-using-only-iptables-on-an-ubuntu-vps
    # create chains
    my_call(['-N', 'KNOCKING'])

    # build chain for each knocking port
    for i in range(0, len(config.SSH_KNOCKING_SEQ)):
        my_call(['-N', 'GATE' + str(i + 1)])
    
    my_call(['-N', 'PASSED'])

    for i in config.SSH_KNOCKING_SEQ:
        my_call(['-A', 'INPUT', '-p', 'tcp', '--dport', str(i), '-j', 'KNOCKING'])

    my_call(['-A', 'INPUT', '-p', 'tcp', '--dport', '22', '-j', 'KNOCKING'])

    my_call(['-A', 'GATE1', '-p', 'tcp', '--dport', str(config.SSH_KNOCKING_SEQ[0]), '-m', 'recent', '--name', 'AUTH1', '--set', '-j', 'DROP'])
    my_call(['-A', 'GATE1', '-j', 'RETURN'])
    
    for i in range(1, len(config.SSH_KNOCKING_SEQ)):
        my_call(['-A', 'GATE' + str(i + 1), '-m', 'recent', '--name', 'AUTH' + str(i), '--remove'])
        my_call(['-A', 'GATE' + str(i + 1), '-p', 'tcp', '--dport', str(config.SSH_KNOCKING_SEQ[i]), '-m', 'recent', '--name', 'AUTH' + str(i + 1), '--set', '-j', 'DROP'])
        my_call(['-A', 'GATE' + str(i + 1), '-j', 'GATE1'])

    my_call(['-A', 'PASSED', '-m', 'recent', '--name', 'AUTH' + str(len(config.SSH_KNOCKING_SEQ)), '--remove'])
    my_call(['-A', 'PASSED', '-p', 'tcp', '--dport', '22', '-j', 'ACCEPT'])
    my_call(['-A', 'PASSED', '-j', 'GATE1'])

    my_call(['-A', 'KNOCKING', '-m', 'recent', '--rcheck', '--seconds', '30', '--name', 'AUTH' + str(len(config.SSH_KNOCKING_SEQ)), '-j', 'PASSED'])

    for i in range(len(config.SSH_KNOCKING_SEQ), 1, -1):
        my_call(['-A', 'KNOCKING', '-m', 'recent', '--rcheck', '--seconds', '10', '--name', 'AUTH' + str(i - 1), '-j', 'GATE' + str(i)])
    
    my_call(['-A', 'KNOCKING', '-j', 'GATE1'])
    
def my_call(arr):
    c = [I] + arr

    if config.DRY_RUN == 'True':
        a = ''
        for i in c:
            a += str(i)
            a += ' '
        
        print(a)
    else:
        call(c)

def read_config(config_path):
    with open(config_path) as config_data:
        config = json.load(config_data, object_hook=lambda d: namedtuple('X', d.keys())(*d.values()))

    return config
        
if __name__=='__main__':
    config_file_path = os.environ.get('FW_PY_CONFIG', '/etc/fw.py.conf')
        
    config = read_config(config_file_path).CONFIG
    build(config)

